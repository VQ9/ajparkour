package us.ajg0702.parkour.game;

/**
 * The difficulty of a {@link us.ajg0702.parkour.game.PkArea PkArea}.
 * @author ajgeiss0702
 *
 */
public enum Difficulty {
	EASY, MEDIUM, HARD, EXPERT, BALANCED
}
